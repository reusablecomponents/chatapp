//
//  ChatViewController.swift
//  FireBaseChatApp
//
//  Created by signity on 06/06/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import Photos
import Firebase
import JSQMessagesViewController
class ChatViewController: JSQMessagesViewController {
  
    //Firebase Properties
    var channelRef: DatabaseReference?
    var channel: Channel? {
        didSet {
            title = channel?.name
        }
    }
    //Send messages and update database
    private lazy var messageRef: DatabaseReference = self.channelRef!.child("messages")
    private var newMessageRefHandle: DatabaseHandle?
    //User Presence / Typing or not
    private lazy var userIsTypingRef: DatabaseReference =
        self.channelRef!.child("typingIndicator").child(self.senderId) // 1
    private var localTyping = false // 2
    var isTyping: Bool {
        get {
            return localTyping
        }
        set {
            // 3
            localTyping = newValue
            userIsTypingRef.setValue(newValue)
        }
    }
    //Querying for Typing Users
    private lazy var usersTypingQuery: DatabaseQuery =
        self.channelRef!.child("typingIndicator").queryOrderedByValue().queryEqual(toValue: true)
    //Send Image and sotore on firebase database
    lazy var storageRef: StorageReference = Storage.storage().reference(forURL: ConstantModel.firebaseImageStorageUrl)
    private let imageURLNotSetKey = "NOTSET"
    let imagePicker = UIImagePickerController()
    private var updatedMessageRefHandle: DatabaseHandle?
    //JSQMessageVC Properties
    var messages = [JSQMessage]()
    var messagesId = [String]()
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    //JSQMessageVC display Image message
    private var photoMessageMap = [String: JSQPhotoMediaItem]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.senderId = Auth.auth().currentUser?.uid
        // No avatars
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        //Get all send/recived messages (message history)
//        observeMessages()
        getdataByFixDate()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        observeTyping()
    }
    //MARK: remove ref of data
    deinit {
        if let refHandle = newMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
        
        if let refHandle = updatedMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
    }
    //MARK: Creating Messages or Add a new Message using JSQMessages
    private func addMessage(withId id: String, name: String, text: String) {
        if let message = JSQMessage(senderId: id, displayName: name, text: text) {
            messages.append(message)
        }
    }
    //MARK: Creating Photo Messages using JSQMessages
    private func addPhotoMessage(withId id: String, key: String, mediaItem: JSQPhotoMediaItem) {
        if let message = JSQMessage(senderId: id, displayName: "", media: mediaItem) {
            messages.append(message)
            
            if (mediaItem.image == nil) {
                photoMessageMap[key] = mediaItem
            }
            
            collectionView.reloadData()
        }
    }
    //MARk: Remove Message
    
    //MARK: press Accessory button Action override from JSQMessages
    override func didPressAccessoryButton(_ sender: UIButton) {
        GetCameraPhotoLibModel.shared.getCameraPhotos(picker: imagePicker, sender: self)
        selectedImageHandler = {image, imgExtension in
            self.imagePicker.dismiss(animated: true, completion: nil)
            self.uploadImageDataFirebase(imageMessage: image!, imgExtension: imgExtension!)
        }
    }
    //MARK: Send button Action override from JSQMessages
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        //Send message object using firebase
        let itemRef = messageRef.childByAutoId() // 1
        let date = Date()
        let dateTimeStemp = date.millisecondsSince1970
        let messageItem = [ // 2
            "messageId":itemRef.key,
            "senderId": senderId!,
            "senderName": senderDisplayName!,
            "text": text!,
            "createDate": dateTimeStemp
            ] as [String : Any]
        
        itemRef.setValue(messageItem) // 3
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound() // 4
        finishSendingMessage() // 5
        isTyping = false
    }
    //MARK: Send Photo Messages
    func sendPhotoMessage() -> String? {
        let itemRef = messageRef.childByAutoId()
        let date = Date()
        let dateTimeStemp = date.millisecondsSince1970
        let messageItem = [
            "messageId":itemRef.key,
            "photoURL": imageURLNotSetKey,
            "senderId": senderId!,
            "createDate": dateTimeStemp
            ] as [String : Any]
        
        itemRef.setValue(messageItem)
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        finishSendingMessage()
        return itemRef.key
    }
   
    //MARK: Update Image url on firebase once uploaded sucessfully
    func setImageURL(_ url: String, forPhotoMessageWithKey key: String) {
        let itemRef = messageRef.child(key)
        itemRef.updateChildValues(["photoURL": url])
    }
    //MARk: Remove Message
    @objc func copyChatMessage(indexpath: IndexPath){
    }
   @objc func removeChatMessage(indexpath: IndexPath){
       let message = messages[indexpath.item]
            if message.isMediaMessage == false {
                messageRef.child(messagesId[indexpath.item]).removeValue(completionBlock: { (error, ref) in
                    
                    if error != nil{
                        
                        print(error!.localizedDescription)
                        
                    }else {
                        
                        self.messages.remove(at: indexpath.item)
                        self.messagesId.remove(at: indexpath.item)
                        self.collectionView.deleteItems(at: [indexpath])
                        self.collectionView.reloadData()
                    }
                })
            } else {
            
                let imageRef = Storage.storage().reference().child((Auth.auth().currentUser?.uid)!).child(messagesId[indexpath.item])
            imageRef.delete(completion: { (error) in
                
                if error != nil {
                    
                    print("no succcess")
                    print(error!.localizedDescription)
                    
                }else {
                    
                    self.messageRef.child(self.messagesId[indexpath.item]).removeValue(completionBlock: { (error, ref) in
                        
                        if error != nil{
                            
                            print(error!.localizedDescription)
                            
                        }else {
                            
                            self.messages.remove(at: indexpath.item)
                            self.messagesId.remove(at: indexpath.item)
                            self.collectionView.deleteItems(at: [indexpath])
                            self.collectionView.reloadData()
                        }
                    })
                }
                
            })
        }
    }
    //MARK:Knowing When a User is Typing and Querying for Typing Users
    private func observeTyping() {
        let typingIndicatorRef = channelRef!.child("typingIndicator")
        userIsTypingRef = typingIndicatorRef.child(senderId)
        userIsTypingRef.onDisconnectRemoveValue()
        // 1
        usersTypingQuery.observe(.value) { (data: DataSnapshot) in
            // 2 You're the only one typing, don't show the indicator
            if data.childrenCount == 1 && self.isTyping {
                return
            }
            
            // 3 Are there others typing?
            self.showTypingIndicator = data.childrenCount > 0
            self.scrollToBottom(animated: true)
        }
    }
    //MARK: Synchronizing the Data Source
    func getdataByFixDate(){
        messageRef = channelRef!.child("messages")
        var date = Date()
        let calendar = Calendar.current
        date = calendar.date(byAdding: .minute, value: -5, to: date)!
        let dateTimeStemp = date.millisecondsSince1970
        messageRef.queryOrdered(byChild: "createDate").queryStarting(atValue: dateTimeStemp)
            .observe(.childAdded, with: { (snapshot) -> Void in
                // 3
                let messageData = snapshot.value as! Dictionary<String, Any>
                
                if let id = messageData["senderId"] as? String, let name = messageData["senderName"] as? String, let text = messageData["text"] as? String, text.count > 0 {
                    // 4
                    self.addMessage(withId: id, name: name, text: text)
                    self.messagesId.append(messageData["messageId"] as? String ?? "" )
                    // 5 inform JSQMessagesViewController that a message has been received.
                    self.finishReceivingMessage()
                }else if let id = messageData["senderId"] as? String,
                    let photoURL = messageData["photoURL"] as? String { // 1
                    // 2
                    if let mediaItem = JSQPhotoMediaItem(maskAsOutgoing: id == self.senderId) {
                        // 3
                        self.addPhotoMessage(withId: id, key: snapshot.key, mediaItem: mediaItem)
                        self.messagesId.append(messageData["messageId"] as? String ?? "")
                        // 4
                        if photoURL.hasPrefix("gs://") {
                            self.fetchImageDataAtURL(photoURL, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: nil)
                        }
                    }
                } else {
                    print("Error! Could not decode message data")
                }
            })
    }
    private func observeMessages() {
        messageRef = channelRef!.child("messages")
        // 1.
        let messageQuery = messageRef.queryLimited(toLast:25)
        
        // 2. We can use the observe method to listen for new
        // messages being written to the Firebase DB
        newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
            // 3
            let messageData = snapshot.value as! Dictionary<String, String>
            
            if let id = messageData["senderId"] as String!, let name = messageData["senderName"] as String!, let text = messageData["text"] as String!, text.count > 0 {
                // 4
                self.addMessage(withId: id, name: name, text: text)
                self.messagesId.append(messageData["messageId"] ?? "")
                // 5 inform JSQMessagesViewController that a message has been received.
                self.finishReceivingMessage()
            }else if let id = messageData["senderId"] as String!,
                let photoURL = messageData["photoURL"] as String! { // 1
                // 2
                if let mediaItem = JSQPhotoMediaItem(maskAsOutgoing: id == self.senderId) {
                    // 3
                    self.addPhotoMessage(withId: id, key: snapshot.key, mediaItem: mediaItem)
                    self.messagesId.append(messageData["messageId"] ?? "")
                    // 4
                    if photoURL.hasPrefix("gs://") {
                        self.fetchImageDataAtURL(photoURL, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: nil)
                    }
                }
            } else {
                print("Error! Could not decode message data")
            }
        })
        // We can also use the observer method to listen for
        // changes to existing messages.
        // We use this to be notified when a photo has been stored
        // to the Firebase Storage, so we can update the message data
        updatedMessageRefHandle = messageRef.observe(.childChanged, with: { (snapshot) in
            let key = snapshot.key
            let messageData = snapshot.value as! Dictionary<String, String> // 1
            
            if let photoURL = messageData["photoURL"] as String! { // 2
                // The photo has been updated.
                if let mediaItem = self.photoMessageMap[key] { // 3
                    self.fetchImageDataAtURL(photoURL, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: key) // 4
                }
            }
        })
    }
    private func fetchImageDataAtURL(_ photoURL: String, forMediaItem mediaItem: JSQPhotoMediaItem, clearsPhotoMessageMapOnSuccessForKey key: String?) {
        // 1
        let storageRef = Storage.storage().reference(forURL: photoURL)
        
        // 2
        storageRef.getData(maxSize: INT64_MAX) { (data, error) in
            if let error = error {
                print("Error downloading image data: \(error)")
                return
            }
            // 3
            storageRef.getMetadata(completion: { (metadata, metadataErr) in
                if let error = metadataErr {
                    print("Error downloading metadata: \(error)")
                    return
                }
                // 4
                if (metadata?.contentType == "image/gif") {
                    mediaItem.image = UIImage.gifImageWithData(data: data! as NSData)
                } else {
                    mediaItem.image = UIImage.init(data: data!)
                }
                self.collectionView.reloadData()
                
                // 5
                guard key != nil else {
                    return
                }
                self.photoMessageMap.removeValue(forKey: key!)
            })
        }
    }
     // MARK: JSQMessagesCollectionViewDelegate
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    //MARK: JSQMessagesCollectionViewDataSource
    //MARK: Setting the Bubble Images
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item] // 1
        if message.senderId == senderId { // 2
            return outgoingBubbleImageView
        } else { // 3
            return incomingBubbleImageView
        }
    }
    //MARK:Message Bubble Text
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        if message.senderId == senderId {
            cell.textView?.textColor = UIColor.white
        } else {
            cell.textView?.textColor = UIColor.black
        }
        cell.textView?.isUserInteractionEnabled = false
        return cell
    }
    //MARK: Removing the Avatars from JSQMessages
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        self.showActionSheet(indexPath: indexPath)
    }
   
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapCellAt indexPath: IndexPath!, touchLocation: CGPoint) {
        self.showActionSheet(indexPath: indexPath)
    }
    func showActionSheet(indexPath: IndexPath){
        let actionSheetController: UIAlertController = UIAlertController(title: ConstantModel.btnText.btnSelect, message: "", preferredStyle: .actionSheet)
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModel.btnText.btnCancel, style: .cancel, handler: nil))
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModel.btnText.btnDelete, style: .default, handler: { action in
            self.removeChatMessage(indexpath: indexPath as IndexPath)
        }))
        actionSheetController.addAction(UIAlertAction.init(title: ConstantModel.btnText.btnCopy, style: .default, handler: { action in
            UIPasteboard.general.string = self.messages[indexPath.item].text as String
        }))
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //MARK: Set Message Bubble Colors in JSQMessages (outgoing and incoming. Outgoing messages are displayed to the right and incoming messages on the left.)
    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    //Mark: Knowing When a User is Typing
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        // If the text is not empty, the user is typing
        isTyping = textView.text != ""
    }
    func uploadImageDataFirebase(imageMessage: UIImage,imgExtension: String){
        // Handle picking a Photo from the Photo Library
        // 1
        if let key = sendPhotoMessage() {
            let timeIntervalVal = Int(Date.timeIntervalSinceReferenceDate * 1000)
            let path = (Auth.auth().currentUser?.uid)! + String(timeIntervalVal) + imgExtension
            // 2
            let image_data = UIImagePNGRepresentation(imageMessage)
            self.storageRef.child(path).putData(image_data!, metadata: nil, completion: { (metadata, error) in
                if let error = error {
                    print("Error uploading photo: \(error.localizedDescription)")
                    return
                }
                // 3
                self.setImageURL(self.storageRef.child((metadata?.path)!).description, forPhotoMessageWithKey: key)
            })
        }
    }
    func uploadandGetFilePath(fileUrl: NSURL){
        // Handle picking a Photo from the Photo Library
        // 1
        if let key = sendPhotoMessage() {
            let timeIntervalVal = Int(Date.timeIntervalSinceReferenceDate * 1000)
            let path = (Auth.auth().currentUser?.uid)! + String(timeIntervalVal) + fileUrl.lastPathComponent!
            // 2
            self.storageRef.child(path).putFile(from: fileUrl as URL, metadata: nil) { (metadata, error) in
                if let error = error {
                    print("Error uploading photo: \(error.localizedDescription)")
                    return
                }
                // 3
                self.setImageURL(self.storageRef.child((metadata?.path)!).description, forPhotoMessageWithKey: key)
            }
        }
    }
}

