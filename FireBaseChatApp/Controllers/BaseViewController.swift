//
//  ViewController.swift
//  FireBaseChatApp
//
//  Created by signity on 06/06/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import Firebase
class BaseViewController: UIViewController {
    // MARK: -  IBOutlet 
    @IBOutlet weak var userNameField       : CustomAnimatableTextfield!
    
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: -  IBOutlet Action 
    @IBAction func SubmitButtonAction(_ sender: UIButton) {
        EventManager.showloader()
        if userNameField?.text != "" { // 1
            Auth.auth().signInAnonymously(completion: { (user, error) in // 2
                if let err = error { // 3
                    print(err.localizedDescription)
                    return
                }
                EventManager.hideloader()
                self.performSegue(withIdentifier: "StartChatListView", sender: nil) // 4
            })
        }
    }
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        let channelVc = segue.destination as! ChannelListViewController // 1
        channelVc.senderDisplayName = userNameField?.text // 2
    }
  
}

