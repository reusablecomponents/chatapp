//
//  CreateChannelCell.swift
//  FireBaseChatApp
//
//  Created by signity on 06/06/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit

class CreateChannelCell: UITableViewCell {
    @IBOutlet weak var newChannelNameField: UITextField!
    @IBOutlet weak var createChannelButton: UIButton!
}
