//
//  ChannelListViewController.swift
//  FireBaseChatApp
//
//  Created by signity on 06/06/18.
//  Copyright © 2018 signity. All rights reserved.
//

import UIKit
import Firebase

enum Section: Int {
    case createNewChannelSection = 0
    case currentChannelsSection
}
class ChannelListViewController: UITableViewController {
    // MARK: Properties
    var senderDisplayName: String? // 1
    var newChannelTextField: UITextField? // 2
    private var channels: [Channel] = [] // 3
    private lazy var channelRef: DatabaseReference = Database.database().reference().child("channels")
    private var channelRefHandle: DatabaseHandle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        EventManager.showloader()
        observeChannels()
        // Do any additional setup after loading the view.
    }
    deinit {
        if let refHandle = channelRefHandle {
            channelRef.removeObserver(withHandle: refHandle)
        }
    }
    // MARK: Firebase related methods
    private func observeChannels() {
        // Use the observe method to listen for new
        // channels being written to the Firebase DB
        channelRefHandle = channelRef.queryOrdered(byChild: "userId").queryEqual(toValue: "uid_1").observe(.childAdded, with: { (snapshot) -> Void in // 1
            let channelData = snapshot.value as! Dictionary<String, AnyObject> // 2
            let id = snapshot.key
            if let name = channelData["name"] as! String!, name.count > 0 { // 3
                self.channels.append(Channel(id: id, name: name))
                self.tableView.reloadData()
            } else {
                EventManager.showAlert(alertMessage: "Error! Could not decode channel data", btn1Tit: "Ok", btn2Tit: nil, sender: self, action: nil, style: 0)
                print("Error! Could not decode channel data")
            }
            EventManager.hideloader()
        })
    }
//    private func observeChannels() {
//        // Use the observe method to listen for new
//        // channels being written to the Firebase DB
//        channelRefHandle = channelRef.observe(.childAdded, with: { (snapshot) -> Void in // 1
//            let channelData = snapshot.value as! Dictionary<String, AnyObject> // 2
//            let id = snapshot.key
//            if let name = channelData["name"] as! String!, name.count > 0 { // 3
//                self.channels.append(Channel(id: id, name: name))
//                self.tableView.reloadData()
//            } else {
//                EventManager.showAlert(alertMessage: "Error! Could not decode channel data", btn1Tit: "Ok", btn2Tit: nil, sender: self, action: nil, style: 0)
//                print("Error! Could not decode channel data")
//            }
//            EventManager.hideloader()
//        })
//    }
    // MARK :IBActions
    @IBAction func createGeneralChannel(_ sender: AnyObject) {
        if let name = newChannelTextField?.text { // 1
            //Set channel object using firebase
            let newChannelRef = channelRef.childByAutoId() // 2
            let channelItem = [ // 3
                "name": name
            ]
            newChannelRef.setValue(channelItem) // 4
            newChannelTextField?.text = ""
        } else {
            
        }
    }
    // MARK :IBActions
    @IBAction func createChannel(_ sender: AnyObject) {
        if let name = newChannelTextField?.text { // 1
            //Set channel object using firebase
            let newChannelRef = channelRef.childByAutoId() // 2
            let channelItem = [ // 3
                "name": name,
                "userId":"uid_0"
            ]
            newChannelRef.setValue(channelItem) // 4
            newChannelTextField?.text = ""
        } else {
            
        }
    }
    func removeChannel(indexPath: NSIndexPath){
        channelRef.child(channels[indexPath.row].id).removeValue(completionBlock: { (error, ref) in
            if error != nil{
                print(error!.localizedDescription)
            }else {
                self.channels.remove(at: indexPath.item)
                self.tableView.reloadData()
            }
        })
        
    }
    func clearChannelData(indexPath: NSIndexPath){
        channelRef.child(channels[indexPath.row].id).child("messages").removeValue(completionBlock:  { (error, ref) in
            if error != nil{
                print(error!.localizedDescription)
            }else {
                self.tableView.reloadData()
            } 
        })
    }
    // MARK: UITableViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2 // 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { // 2
        if let currentSection: Section = Section(rawValue: section) {
            switch currentSection {
            case .createNewChannelSection:
                return 1
            case .currentChannelsSection:
                return channels.count
            }
        } else {
            return 0
        }
    }
    // 3
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier = (indexPath as NSIndexPath).section == Section.createNewChannelSection.rawValue ? "NewChannel" : "ExistingChannel"
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        
        if (indexPath as NSIndexPath).section == Section.createNewChannelSection.rawValue {
            if let createNewChannelCell = cell as? CreateChannelCell {
                newChannelTextField = createNewChannelCell.newChannelNameField
            }
        } else if (indexPath as NSIndexPath).section == Section.currentChannelsSection.rawValue {
            cell.textLabel?.text = channels[(indexPath as NSIndexPath).row].name
        }
        
        return cell
    }
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
   
    
    let delete = UITableViewRowAction(style: .normal, title: "Delete") { (action, indexPath) in
        // code to delete channel the todo goes here
        self.removeChannel(indexPath: indexPath as NSIndexPath)
    }
    let clear = UITableViewRowAction(style: .normal, title: "Clear") { (action, indexPath) in
            // code to clear channel chat the todo goes here
        self.clearChannelData(indexPath: indexPath as NSIndexPath)
    }
    delete.backgroundColor = UIColor.red
    clear.backgroundColor = UIColor.gray
    return [delete, clear]
    }
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == Section.currentChannelsSection.rawValue {
            let channel = channels[(indexPath as NSIndexPath).row]
            self.performSegue(withIdentifier: "ShowChatView", sender: channel)
        }
    }

    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let channel = sender as? Channel {
            let chatVc = segue.destination as! ChatViewController
            chatVc.senderDisplayName = senderDisplayName
            chatVc.channel = channel
            chatVc.channelRef = channelRef.child(channel.id)
            chatVc.hidesBottomBarWhenPushed = true
        }
    }
}
