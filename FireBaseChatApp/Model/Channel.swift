
//  Created by Anisha
//  Copyright © 2018 Anisha. All rights reserved.
internal class Channel {
  internal let id: String
  internal let name: String
  
  init(id: String, name: String) {
    self.id = id
    self.name = name
  }
}
