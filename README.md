Update the podfiles for YPImagePicker and Create a call back for handling "Cancel" event from pickerview

Add following code in "YPImagePicker.swift" class
public var didClose: (() -> Void)? // Declare a callback

 In "viewDidLoad" define the callback as follow
 picker.didClose = {
 self.didClose?()
 }

